import * as types from './muations_types'
import { shuffle } from 'common/js/util'
import { play_mode } from 'common/js/config'
import { saveSearch, clearSearch, DelSearch, savePlayHistoryList, saveFavoritesList, DelFavorite } from 'common/js/cache'
function getIndex (list, song) {
  return list.findIndex( v => v.id === song.id)
}
export function handleSelectPlay ({ commit, state }, { index, list }) {
    commit(types.SET_SEQUENCELIST, list)
    if (state.mode === play_mode.random) {
      let randomList = shuffle(list)
      commit(types.SET_PLAYLIST,randomList)
      index = getIndex(randomList, list[index])
    } else {
      commit(types.SET_PLAYLIST, list)
    }
    commit(types.SET_CURRENTINDEX, index)
    commit(types.SET_PLAYING, true)
    commit(types.SET_FULLSCREEN, true)
}

export function handelRandomPlay ({ commit }, { list }) {
    commit(types.SET_SEQUENCELIST, list)
    commit(types.SET_MODE, play_mode.random)
    let randomList = shuffle(list)
    commit(types.SET_PLAYLIST, randomList)
    commit(types.SET_PLAYING, true)
    commit(types.SET_FULLSCREEN, true)
    commit(types.SET_CURRENTINDEX, 0)
}

export function handleAddPlay ( {commit, state}, { song, isFullScreen } ) {
    let playList = state.playList,
    sequenceList = state.playList
    let flag = getIndex(playList, song) === -1
    if (flag) {
      playList.push(song)
    }
     commit(types.SET_CURRENTINDEX, !flag ? getIndex(playList, song) : (playList.length - 1))
     commit(types.SET_PLAYLIST, playList)
     commit(types.SET_SEQUENCELIST, sequenceList)
     commit(types.SET_PLAYING, true)
     if (isFullScreen) commit(types.SET_FULLSCREEN, true)

}

export function saveSearchHistory ({ commit },  query ) {
 commit( types.SET_SEARCHHISTORY,saveSearch(query))
}

export function clearSearchHistory ({ commit }) {
  commit(types.SET_SEARCHHISTORY, [])
  clearSearch()
}
export function DelSearchHistory ({ commit }, item) {
  commit(types.SET_SEARCHHISTORY, DelSearch(item))
}

export function clearPlayList ({ commit }) {
  commit(types.SET_PLAYING, false)
  commit(types.SET_FULLSCREEN, null)
  commit(types.SET_CURRENTINDEX, -1)
  commit(types.SET_PLAYLIST, [])
  commit(types.SET_SEQUENCELIST, [])
}

export function handelDeletePlay ({commit, state}, song) {
  let newArray1 = state.playList.slice(),
  newArray2 = state.sequenceList.slice(),
  currentIndex = state.currentIndex
  let pIndex = getIndex(newArray1, song),
  sIndex = getIndex(newArray2, song)
  newArray1.splice(pIndex, 1)
  newArray2.splice(sIndex, 1)
  if (currentIndex > pIndex || currentIndex === newArray1.length) currentIndex --
  commit(types.SET_PLAYLIST, newArray1)
  commit(types.SET_SEQUENCELIST, newArray2)
  commit(types.SET_CURRENTINDEX, currentIndex)
  if (!newArray1.length) {
    commit(types.SET_PLAYING, false)
    commit(types.SET_CURRENTINDEX, -1)
  }
}

export function savePlayHistory ({ commit }, song) {
    commit(types.SET_PLAYHISTORY, savePlayHistoryList(song))
}

export function saveFavorites ({ commit }, song) {
  commit(types.SET_FAVORITE, saveFavoritesList(song))
}

export function deleteFavorite ({ commit }, song) {
  commit(types.SET_FAVORITE, DelFavorite(song))
}