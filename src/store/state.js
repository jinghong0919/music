import { loadSearch, loadPlayHistory, loadFavorite } from 'common/js/cache'
import { play_mode } from 'common/js/config'
const state = {
  singer: {},
  playing: false,
  fullScreen: false,
  playList: [],
  sequenceList: [],
  mode: play_mode.sequence,
  currentIndex: -1,
  disc: {},
  topList: {},
  searchHistory: loadSearch(),
  playHistory: loadPlayHistory(),
  Favorites: loadFavorite()
}
export default state