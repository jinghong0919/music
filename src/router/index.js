import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/recommend'
  },
  {
    path: '/recommend',
    name: 'recommend',
    component: () => import('@/components/recommend/recommend'),
    children: [
      {
        path: ':id',
        component: () => import('@/components/disc/disc')
      }
    ]
  },
  {
    path: '/rank',
    name: 'rank',
    component: () => import('@/components/rank/rank'),
    children: [
      {
        path: ':id',
        component:() => import('@/components/topList/topList')
      }
    ]
  },
  {
    path: '/singer',
    name: 'singer',
    component: () => import('@/components/singer/singer'),
    children: [
     { path: ':id',
      component: () => import('@/components/singer-detail/singer-detail')}
    ]
  },
  {
    path: '/search',
    name: 'search',
    component: () => import('@/components/search/search'),
    children: [
      { path:':id',
       component: () => import('@/components/singer-detail/singer-detail') }
    ]
  },
  {
    path: '/user',
    name: 'user',
    component: () => import('@/components/user/user')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
