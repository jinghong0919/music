import jsonp from 'common/js/jsonp'
import { commonParams, options } from '@/api/common'
import axios from 'axios'
export async function getFocus () {
    let url = '/api/getfocus'
    let data = Object.assign({}, commonParams, {
        '-': 'recom9127706050023798',
        platform: 'yqq.json',
        uin: 0,
        needNewCode: 1,
        g_tk: 1028852985,
        loginUin: 0,
        hostUid: 0,
        needNewCode: 0,
        data: '{"focus":{"module":"QQMusic.MusichallServer","method":"GetFocus","param":{}}}'
    })
    return await axios.get(url, { params: data })
}
export async function getDiscList () {
    let url = '/api/getDiscList'
    let data = Object.assign({}, commonParams, {
        rnd: Math.random(),
        notice: 0,
        platform: 'yqq.json',
        sin:0,
        ein:15,
        needNewCode:0,
        loginUin:0,
        hostUin:0,
        sortId: 5,
        categoryId: 10000000,
        format: 'json'
    })
    return await axios.get(url, { params:data })
}

export function getSongList (disstid) {
    let url = '/api/getDiscSongList'
    let data = Object.assign({}, commonParams, {
        disstid,
        type: 1,
        json: 1,
        utf8: 1,
        onlysong: 0,
        platform: 'yqq',
        hostUid: 0,
        needNewCode: 0
    })
    return jsonp(url, data, options)
}