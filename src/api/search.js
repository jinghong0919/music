import jsonp from 'common/js/jsonp'
import { commonParams, options } from '@/api/common'

export function getHots () {
    let url = 'https://c.y.qq.com/splcloud/fcgi-bin/gethotkey.fcg'
    let data = Object.assign({}, commonParams, {
        uin: 0,
        needNewCode: 1,
        platform: 'h5'
    })
    return jsonp(url, data, options)
}

export function getSearch (query, page, zhida, num = 20) {
    let url = 'https://c.y.qq.com/soso/fcgi-bin/client_search_cp'
    let data = Object.assign({}, commonParams, {
        w: query,
        p: page,
        catZhida: zhida ? 1 : 0,
        zhidaqu: 1,
        t: 0,
        flag: 1,
        ie: 'urf-8',
        sem: 1,
        aggr: 0,
        perpage: num,
        n: num,
        uid: 0,
        platform: 'h5',
        ct: 24,
        qqmusic_ver: 1298,
        new_json: 1,
        remoteplace: 'txt.yqq.song',
        searchid: 65103024149790195,
        aggr: 1,
        cr: 1,
        lossless: 0,
        flag_qc: 0,
        loginUin: 0,
        hostUin: 0,
        platform: 'yqq.json',
        needNewCode: 0
    })
    return jsonp(url, data, options)
}