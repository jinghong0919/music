import jsonp from 'common/js/jsonp'
import { commonParams, options } from '@/api/common'
import axios from 'axios'
import { ERROR_OK } from './common'

export  function getRecommendMv () {
    let url ='https://c.y.qq.com/mv/fcgi-bin/getmv_by_tag'
    let data = Object.assign({}, commonParams, {
        g_t: 1028852985,
        loginUin: 0,
        hostUin: 0,
        outCharset: 'GB2312',
        platform: 'yqq.json',
        needNewCode: 0,
        cmd: 'shoubo',
        lan: 'all'
    })
    return jsonp(url, data, options)
}

export async function getMvInfo (list) {
    let url = '/api/getMvUrl'
    let data = Object.assign({}, commonParams, {
        g_t: 1668790249,
        loginUin: 0,
        hostUin: 0,
        platform: 'yqq.json',
        needNewCode: 0,
        '-': 'getUCGI7418039955111058',
        data: `{"comm":{"ct":24,"cv":4747474},"mvinfo":{"module":"video.VideoDataServer","method":"get_video_info_batch","param":{"vidlist":${JSON.stringify(getVid(list))},"required":["vid","cover_pic","singers","msg","name","desc","playcnt","pubdate"]}}}`
    })
    return await axios.get(url, { params: data })
}

export async function getMvUrl (list) {
    let url = '/api/getMvUrl'
    let data = Object.assign({}, commonParams, {
        data: `{"getMvUrl":{"module":"gosrf.Stream.MvUrlProxy","method":"GetMvUrls","param":{"vids":${JSON.stringify(getVid(list))},"request_typet":10001}}}`,
        g_tk: 1028852985,
        loginUin: 0,
        hostUin: 0,
        outCharset: 'GB2312',
        platform: 'yqq',
        needNewCode: 0
    })
    return _normalizeMves(list, (await axios.get(url,{ params: data })).data.getMvUrl.data)
}

function _normalizeMves (list, urls) {
    list.forEach(v => v.MvUrl = urls[v.vid])
    return list
}

function getVid (list) {
    let res = []
    list.forEach(v => res.push(v.vid))
    return res
}