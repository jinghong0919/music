import jsonp from 'common/js/jsonp'
import { commonParams, options } from '@/api/common'

export function getRank() {
  let url = 'https://c.y.qq.com/v8/fcg-bin/fcg_myqq_toplist.fcg'
  let data = Object.assign({}, commonParams, {
      platform: 'h5',
      needNewCode: 1,
      uin: 0
  })
  return jsonp(url, data, options)
}

export function getSongList (topid) {
  let url = 'https://c.y.qq.com/v8/fcg-bin/fcg_v8_toplist_cp.fcg'
  let data = Object.assign({}, commonParams, {
        page: 'detail',
        type: 'top',
        tpl: 3,
        platform: 'h5',
        topid,
        needNewCode: 1
  })
  return jsonp(url, data, options)
}