import { commonParams } from './common'
import axios from 'axios'
import { ERROR_OK } from './common'
import { getUid } from 'common/js/uid'

const debug = process.env.NODE_ENV !== 'production'

export function getSongsUrl (songs) {
    // const url = debug ? '/api/getPurlUrl' : 'http://ustbhuangyi.com/music/api/getPurlUrl'
    const url = '/api/getPurlUrl'


    let mids = [],
    types = []

    songs.forEach(song => {
        mids.push(song.mid)
        types.push(0)
    })

    const urlMid =  genUrlMid(mids,types)

    const data = Object.assign({}, commonParams, {
        g_tk: 5381,
        format: 'json',
        platform: 'h5',
        needNewCode: 1,
        uin: 0
    })

     return new Promise((resolve, reject) => {
         let tryTime = 10

        async function request () {
            let data = await axios.post(url, {
                comm: data,
                req_0: urlMid
              })
             return data.data.code === ERROR_OK ? (() => {
                let urlMid = data.data.req_0
                urlMid && urlMid.code === ERROR_OK ? (() => {
                    const purlMap = {}
                    urlMid.data.midurlinfo.forEach(item => {
                        if (item.purl) {
                            purlMap[item.songmid] = item.purl
                        }
                    })
                    if (Object.keys(purlMap).length > 0) {
                        resolve(purlMap)
                    } else {
                        setTimeout(retry,1000)
                    }
                })() : setTimeout(retry,1000)
             })() : setTimeout(retry,1000)
         }
         function retry () {
            if (--tryTime >= 0) {
              request()
            } else {
              reject(new Error('Can not get the songs url'))
              alert('歌曲获取失败')
            }
          }
          request()
     })
}

function genUrlMid (mids, types) {
    const guid = getUid()
    return {
      module: 'vkey.GetVkeyServer',
      method: 'CgiGetVkey',
      param: {
        guid,
        songmid: mids,
        songtype: types,
        uin: '0',
        loginflag: 0,
        platform: '23'
      }
    }
  }
export async function getlyric(songmid) {
  let url = '/api/getlyric'
  let data = Object.assign({}, commonParams, {
    songmid:songmid,
    platform: 'yqq',
    hostUin:0,
    needNewCode: 0,
    categoryId:10000000,
    pcachetime: +new Date(),
    format: 'json'
  })
  return (await axios.get(url, {
    params: data
  }))
}