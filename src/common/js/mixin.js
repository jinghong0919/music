import {mapGetters, mapActions, mapMutations} from 'vuex'
import { getRecommendMv, getMvUrl, getMvInfo } from '@/api/mv'
import { ERROR_OK } from '@/api/common'
export const playListMixin = {
  computed: {
    ...mapGetters(['playList'])
  },
  activated () {
    this.handlePlayList()
  },
  mounted () {
    this.handlePlayList()
  },
  watch: {
    playList () {
      this.handlePlayList()
    }
  },
  methods: {
    handlePlayList () {
      return new Error(' no handlePlayList method ')
    }
  }
}


export const ToggleShow = {
  methods: {
    hide () {
      this.showFlag = false
    },
    show () {
      this.showFlag = true
    }
  },
  data () {
    return {
      showFlag: false
    }
  }
}

export const Favorite = {
  computed: {
    ...mapGetters(['Favorite'])
  },
  methods: {
    ...mapActions(['saveFavorites', 'deleteFavorite']),
    isFavorite (song) {
      return this.Favorite.findIndex(v => v.id === song.id) !== -1
    },
    ToggleFavorite (song) {
      return this.isFavorite(song) ? this.deleteFavorite(song) : this.saveFavorites(song)
    },
    getFavoriteIcon (song) {
      return this.isFavorite(song) ? 'icon-favorite' : 'icon-not-favorite'
    }
  }
}


export const DateFormat = {
  methods: {
    _format(time) {
      time = time | 0
      let minute = time / 60 | 0,
      second = time % 60
      return `${this._pad(minute)}:${this._pad(second)}`
    },
    _pad (num, n = 2) {
      let len = num.toString().length
      while (len < n) {
        num = '0' + num
        len++
      }
      return num
    }
  }
}

export const selectMv = {
  methods: {
    showMv (vid) {
      this.$emit('selectMv',vid)
    }
  }
}

export const showMv = {
  data () {
    return {
      vid: ''
    }
  },
  methods: {
    selectMv (vid) {
      this.$refs.mv.show()
      this.vid = vid
    }
  }
}

const MV_LENGTH_MAX = 5
export const MvCommonFn = {
  methods: {
    _normailizeOptions (list) {
      let res = []
      list.forEach(v => {
        res.push({ 
          sources:[{src: this.ForUrl(v.MvUrl.mp4), type: 'video/mp4'}],
          poster: v.picurl || v.cover_pic,
          width: window.innerWidth,
          muted: false,
          language: 'zh-CN',
          aspectRatio: '16:9',
          playbackRates: [0.7, 1.0, 1.5, 2.0],
          loop: false,
          autoplay: false,
          notSupportedMessage: '此视频暂无法播放，请稍后再试',
          fluid: true
        })
      })
      return res
    },
    async getMvRecommendList(vids) {
      let res = ( await getRecommendMv() )
      if (res.code === ERROR_OK) {
        await this.getMvUrls(res.data.mvlist.slice(0, MV_LENGTH_MAX))
      }
    },
     async getMvUrls (args) {
		this.load = false
        if (!(args.length - 1)) this.mergeMv(args[0] , await this.getOnceMv(args))
        this.mvList = (await getMvUrl(args))
        this.options = this._normailizeOptions(this.mvList)
		this.load = true
     },
     async getOnceMv (arg) {
        return (await getMvInfo(arg)).data.mvinfo.data[arg[0].vid]
     },
     mergeMv (target, mergeObj) {
      return Object.assign(target, mergeObj)
     },
     ForUrl (list) {
      let res = []
      list.forEach(v => {
        if (!v.freeflow_url.length) return
        res.push(...v.freeflow_url)
      })
      return res[Math.min(Math.floor(Math.random() * res.length), Math.max(0,res.length))]
    },
  },
  data () {
    return {
      mvList: [],
      options: [],
	  load: true
    }
  }
}
