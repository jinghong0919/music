import { getSongsUrl } from '@/api/song'
import { getlyric } from '@/api/song'
import { ERROR_OK } from '../../api/common'
import { Base64 } from 'js-base64'
export default class Song {
    constructor ({id, mid, singer, name, album, duration, image, url, vid}) {
        this.id = id
        this.mid = mid
        this.singer = singer
        this.name = name
        this.album = album
        this.duration = duration
        this.image = image
        this.url = url
        this.filename = `C400${this.mid}.m4a`
        this.vid = vid
    }
    async getlyric () {
      if (this.lyric) return
      let res = (await getlyric(this.mid)).data
      if (res.code === ERROR_OK ) {
        this.lyric = Base64.decode(res.lyric)
      } else {
        return 'no lyric'
      }
    }
}

export function createSong(musicData) {
    return new Song({
      vid: musicData.vid || (musicData.mv && musicData.mv.vid),
      id: musicData.songid || musicData.id,
      mid: musicData.songmid || musicData.mid,
      singer: filterSinger(musicData.singer),
      name : musicData.songname || musicData.name,
      album: musicData.albumname || musicData.album.name,
      duration: musicData.interval || 0,
      image: musicData.albummid || musicData.album.mid ? `https://y.gtimg.cn/music/photo_new/T002R300x300M000${musicData.albummid || musicData.album.mid}.jpg?max_age=2592000` : null
    })
}

export function filterSinger (singers) {
    if (!singers || !singers.length) return ''
    let res = []
    singers.forEach(v => {
        res.push(v.name)
    })
    return res.join('/')
}
 //判断音乐是否有效（收费，版权等等。。）
export function isValidMusic (musicData) {
    return musicData.songid && musicData.albummid && (!musicData.pay || musicData.pay.payalbumprice === 0)
  }
  
  export function processSongsUrl (songs) {
    if (!songs.length) {
      return Promise.resolve(songs)
    }
    return getSongsUrl(songs).then((purlMap) => {
      songs = songs.filter((song) => {
        const purl = purlMap[song.mid]
        if (purl) {
          song.url = purl.indexOf('http') === -1 ? `http://dl.stream.qqmusic.qq.com/${purl}` : purl
          return true
        }
        return false
      })
      return songs
    })
  }