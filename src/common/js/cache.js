import storage from 'good-storage'
const SEARCH_KEY = '_search_',
SEARCH_MAX_LENGTH = 15

const PLAY_KEY = '_play_',
PLAY_MAX_LENGTH = 100

const FAVORITE_KEY = '_favorite_',
FAVORITE_MAX_LENGTH = 1000

function insertArray (arr, query, callback, maxLen) {
    let index = arr.findIndex(callback)
    if (!index) return
    if (index > 0) {
        arr.splice(index, 1)
    }
    arr.unshift(query)
    if (maxLen && arr.length > maxLen) arr.pop()
}

function deleteArray (list, callback) {
    let index = list.findIndex(callback)
    list.splice(index, 1)
    return list
}

export function saveSearch (query) {
  let searches = storage.get(SEARCH_KEY,[])
  insertArray(searches, query, item => item === query ,SEARCH_MAX_LENGTH)
  storage.set(SEARCH_KEY, searches)
  return searches
}
export function loadSearch () {
    return storage.get(SEARCH_KEY,[])
}

export function clearSearch () {
    storage.set(SEARCH_KEY,[])
}
export function DelSearch (item) {
    let searches = storage.get(SEARCH_KEY,[])
    storage.set(SEARCH_KEY, deleteArray(searches,v => v === item))
    return searches
}

export function savePlayHistoryList (song) {
    let arr = storage.get(PLAY_KEY, [])
    insertArray(arr, song, item => item.id === song.id, PLAY_MAX_LENGTH)
    storage.set(PLAY_KEY, arr)
    return arr
}

export function loadPlayHistory () {
    return storage.get(PLAY_KEY, [])
}

export function saveFavoritesList (song) {
    let favorites = storage.get(FAVORITE_KEY, [])
    insertArray(favorites, song, v => v.id === song.id,FAVORITE_MAX_LENGTH)
    storage.set(FAVORITE_KEY, favorites)
    return favorites
}

export function DelFavorite (song) {
    let favorites = storage.get(FAVORITE_KEY, [])
    storage.set(FAVORITE_KEY, deleteArray(favorites,v => v.id === song.id))
    return favorites
}

export function loadFavorite () {
    return storage.get(FAVORITE_KEY, [])
}