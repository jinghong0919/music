import originJsonp from 'jsonp'

export default function (url, data, options) {
    url += (url.indexOf('?') < 0 ? '?' : '&') + param(data)
    return new Promise((resolve, reject) => {
        originJsonp(url, options, (err, data) => {
            if (!err) {
                resolve(data)
            } else {
                reject(err)
            }
        })
    })
}
function param (data) {
    let url = ''
    for (var k in data) {
      url += `&${k}=${encodeURIComponent(data[k] ? data[k] : '')}`
    }
    return url ? url.substring(1) : ''
}