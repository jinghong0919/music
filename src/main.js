import 'babel-polyfill'
import Vue from 'vue'
import store from './store'
import App from './App.vue'
import 'common/styles'
import VueLazyLoad from 'vue-lazyload'
import fastclick from 'fastclick'
import router from './router'
import VueTouchRipple from 'vue-touch-ripple'

// import styles
import 'vue-touch-ripple/dist/vue-touch-ripple.css'

// mount with global
Vue.use(VueTouchRipple, {
  // default global options
  color: '#fff',
  opacity: 0.3,
  speed: 1,
  transition: 'ease'
})
const debug = process.env.NODE_ENV !== 'production'
const vconsole =  debug ? new (require('vconsole')) : undefined

Vue.config.productionTip = false
// fastclick.attach(document.body)
Vue.use(VueLazyLoad, {
  loading: require('common/images/loading.gif'),
  error: require('common/images/default.png')
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
