const express = require('express'),
Router = express.Router(),
axios= require('axios'),
bodyParser = require('body-parser')
const app = express()
app.use(express.static('dist'))

app.use((req, res, next) => {
    try {
        next()
    } catch (error) {
        console.log(error.toString());
    }
})

Router
   .get('/api/getDiscList', async (req, res) => {
     let url = "https://c.y.qq.com/splcloud/fcgi-bin/fcg_get_diss_by_tag.fcg"
     let data = await axios.get(url, {
       headers: {
         referer: "https://y.qq.com/",
         host: "c.y.qq.com"
       },
       params: req.query
     })
     res.send(data.data)
   })
   .get('/api/getDiscSongList', async (req, res) => {
     let url = 'http://c.y.qq.com/qzone/fcg-bin/fcg_ucc_getcdinfo_byids_cp.fcg'
     let data = await axios.get(url, {
       headers: {
         referer: 'http://y.qq.com/',
         host: 'c.y.qq.com'
       },
       params: req.query
     })
     res.send(data.data)
   })
   .post('/api/getPurlUrl', bodyParser.json(), async (req, res) => {
     let url = 'https://u.y.qq.com/cgi-bin/musicu.fcg'
     let data = await axios.post(url, req.body, {
       headers: {
        referer: 'https://y.qq.com/',
        origin: 'https://y.qq.com',
        'Content-type': 'application/x-www-form-urlencoded'
       }
     })
     res.send(data.data)
   })
   .get('/api/getlyric', async (req, res) => {
     let url = 'https://c.y.qq.com/lyric/fcgi-bin/fcg_query_lyric_new.fcg'
     let results = (await axios.get(url,{
       headers: {
         referer: 'https://c.y.qq.com',
         host: 'c.y.qq.com'
       },
       params: req.query
     }))
     res.json(results.data)
     // if (typeof results.lyric === 'string') {
       // let reg = /^\w+\(({[^()]+})\)$/
     //   let metches = results.match(reg)
     //   if (metches) {
     //     res.send(JSON.parse(matches[0]))
     //   }
     // }
   })
   .get('/api/getfocus', async (req, res) => {
     let url = 'https://u.y.qq.com/cgi-bin/musicu.fcg'
     let results = ( await axios.get(url,{
       params: req.query
     }) )
     res.send(results.data)
   })
   .get('/api/getMvUrl', async (req, res) => {
     let url = 'https://u.y.qq.com/cgi-bin/musicu.fcg'
     let results = ( await axios.get(url, {
       params: req.query
     }) )
     res.send(results.data)
   })
   .get('/api/getMvInfo', async (req, res) => {
     let url = 'https://u.y.qq.com/cgi-bin/musicu.fcg'
     let result = ( await axios.get(url, {
       params: req.query
     }) )
     res.send(result.data)
   })
app.use(Router)

app.listen(9091,() => {
    console.log('http://localhost:9091');
})